// TODO Fix filtering by emotions
// TODO Save color mappings between emotions outside of plotting function
// TODO cleanup

// play fragments
var nowpending = '';

var colors = {
    'NEU': '#BCBEC0',
    'FER': '#5e94cf',
    'HAP': '#e7c540',
    'SAD': '#d27e35',
    'SUR': '#88579f',
    'DIS': '#6ab79a',
    'ANG': '#ce4d41',
    'CAL': '#ce34bd'
};

function play(previewurl) {
    window.clearTimeout(nowpending);
    var player = document.createElement("audio");
    player.controls = "controls";
    if (previewurl) {
        player.src = previewurl;
        player.play();
    }
}

function plot_overview(config) {
    var emotions = config['emotions'];
    console.log(config);
    var children = document.querySelector('#emotions').children;

    for (var c = 0, len = children.length; c < len; c++) {
        var emo_class = children[c].getAttribute('class');
        if (emo_class !== null) {
            if (emotions.includes(emo_class)) {
                children[c].style.display = "block"
            } else {
                children[c].style.display = "none"
            }
        }
    }

    var filename = config['corpus'] + '/overview.csv';
    d3.csv(filename, function (data) {
        // Variables
        d3.select("#overview svg").remove();
        var body = d3.select('#overview');

        var highlight_color = "#00449e",
            shaded_color = "#ccc",
            chance_color = "#aaa";

        var dims = window.innerWidth * 0.15,
            height = window.innerHeight * 0.40;
        var m = 50;

        var svg = body.append("svg")
            .attr("width", dims)
            .attr("height", height);


        var xScale = d3.scaleBand()
            .rangeRound([m, dims]);

        xScale.domain(data.map(function (d) {
            return d.model;
        }));

        var yScale = d3.scale.linear()
            .domain([1, 0])
            .range([m, dims - m]);

        // X-axis
        var xAxis = d3.svg.axis()
            .scale(xScale)
            .orient('center');

        // Y-axis
        var yAxis = d3.svg.axis()
            .scale(yScale)
            .ticks(5)
            .orient('left');

        // X-axis
        svg.append('g')
            .attr('class', 'axis')
            .attr('transform', 'translate(0, ' + (dims - m) + ')')
            .call(xAxis)
            .selectAll("text")
            .attr("y", 0)
            .attr("x", 9)
            .attr("dy", ".35em")
            .attr('style', 'font-size: 0.9rem;')
            .attr("transform", "rotate(90)")
            .style("text-anchor", "start");

        // Y-axis
        svg.append('g')
            .attr('class', 'axis')
            .attr('transform', 'translate(' + (m / 2 + 5) + ', 0)')
            .call(yAxis)
            .selectAll("text")
            .attr("x", '-1.8rem')
            .attr('style', 'font-size: 0.8rem;');

        // title
        svg.append("text")
            .attr("x", ((dims / 2)))
            .attr("y", m / 2 - 5)
            .attr("text-anchor", "middle")
            .attr('style', 'font-size: 1rem;')
            .text('SVM recall for representations');

        svg.append("text")
            .attr("x", ((dims / 2)))
            .attr("y", m * 2 / 3)
            .attr("text-anchor", "middle")
            .attr('style', 'font-size: 0.8rem; font-style:italic; color:#999')
            .text('Click on dot to explore representation');

        var valueline = d3.svg.line()
            .x(function (d) {
                return xScale(d.model);
            })
            .y(function (d) {
                return yScale(d.sensitivity);
            });

        svg.append("path")
            .attr("class", "line")
            .attr("d", valueline(data))
            .attr("fill", "none")
            .attr('stroke', shaded_color);

        // Add chance level line
        var chance_level = d3.svg.line()
            .x(function (d) {
                return xScale(d.model);
            })
            .y(function (d) {
                return yScale(1 / config.emotions.length);
            });

        svg.append("path")
            .attr("class", "line")
            .attr("d", chance_level(data))
            .style("stroke-dasharray", ("3, 3"))
            .attr("fill", "none")
            .attr('stroke', chance_color);

        svg.append("text")
            .attr("x", ((dims / 2)))
            .attr("y", height - 10 - yScale(1 / config.emotions.length))
            .attr('style', 'font-size: 0.7rem')
            .attr("fill", chance_color)
            .text('Chance level');

        var get_color = function (d, config) {
            if (d.model === config.model) {
                return highlight_color;
            } else {
                return shaded_color;
            }
        };

        var normal_size = 5, zoomed_size = 10;

        svg.selectAll('circle')
            .data(data)
            .enter()
            .append('circle')
            .attr('cx', function (d) {
                return xScale(d.model)
            })
            .attr('cy', function (d) {
                return yScale(d.sensitivity)
            })
            .attr('r', function (d) {
                return normal_size;
            })
            .attr('fill', function (d) {
                return get_color(d, config)
            })
            .on('mouseover', function (p) {
                d3.select(this)
                    .transition()
                    .duration(100)
                    .attr('r', function (d) {
                        return zoomed_size;
                    })
                    .attr('fill', function (d) {
                        return highlight_color;
                    });

            })
            .on('click', function () {
                d3.select(this).attr('r', function (d) {
                    config.model = d['model'];
                    window.model = d['model'];
                    redraw(config);
                    svg.selectAll('circle')
                        .attr('fill', function (d) {
                            return get_color(d, config)
                        })
                })

            })
            .on('mouseout', function () {
                d3.select(this)
                    .transition()
                    .duration(500)
                    .attr('r', function (d) {
                        return normal_size;
                    })
                    .attr('fill', function (d) {
                        return get_color(d, config)
                    })
            });
    });
}

function redraw(config) {
        var queryDict = {
            'corpus': document.querySelector('#corpus').value
        };
        queryDict = get_GET_params(queryDict);
        queryDict['model'] = window.model;
        set_GET_params(queryDict);

    var filename = config['corpus'] + "/" + config['model'] + '.csv';

    // TODO hide all emotions, except ones available for corpus

    d3.csv(filename, function (data) {
        d3.select("#container svg").remove();
        // Variables
        var body = d3.select('#container');

        var w = window.innerWidth;
        var margin = {top: 50, right: 0.2 * w + 100, bottom: 50, left: 100};
        w = w - margin.left - margin.right;
        var h = window.innerHeight;
        h = h - margin.top - margin.bottom;

        var x_var = "x";
        var y_var = "y";

        var x_min = d3.min(data, function (d) {
            return +d[x_var];
        });
        var x_max = d3.max(data, function (d) {
            return +d[x_var];
        });


        var padding = (x_max - x_min) / 5;
        x_min = x_min - padding;
        x_max = x_max + padding;

        if (x_min < 0) {
            x_min = x_min * -1;
        }

        var x_dom = Math.max(x_max, x_min);

        var xScale = d3.scale.linear()
            .domain([-1 * x_dom, x_dom])
            .range([0, w]);

        var y_min = d3.min(data, function (d) {
            return +d[y_var];
        });
        var y_max = d3.max(data, function (d) {
            return +d[y_var];
        });


        padding = (y_max - y_min) / 5;
        y_min = y_min - padding;
        y_max = y_max + padding;

        if (y_min < 0) {
            y_min = y_min * -1;
        }

        var y_dom = Math.max(y_max, y_min);


        var yScale = d3.scale.linear()
            .domain([-1 * y_dom, y_dom])
            .range([h, 0]);


        // SVG
        var svg = body.append('svg')
            .attr('height', h + margin.top + margin.bottom)
            .attr('width', w + margin.left + margin.right)
            .append('g')
            .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

        // X-axis
        var xAxis = d3.svg.axis()
            .scale(xScale)
            .ticks(5)
            .orient('center');
        // Y-axis
        var yAxis = d3.svg.axis()
            .scale(yScale)
            .ticks(5)
            .orient('left');

        // X-axis
        svg.append('g')
            .attr('class', 'axis')
            .attr('transform', 'translate(0,' + h / 2 + ')')
            .call(xAxis)
            .append('text') // X-axis Label
            .attr('class', 'label')
            .attr('style', 'font-weight:700; font-size: 1.2rem;')
            .attr('y', -30)
            .attr('x', w)
            .attr('dy', '.71em')
            .style('text-anchor', 'end')
            .text('PC1');

        // Y-axis
        svg.append('g')
            .attr('class', 'axis')
            .attr('transform', 'translate(' + w / 2 + ', 0)')
            .call(yAxis)
            .append('text') // y-axis Label
            .attr('class', 'label')
            .attr('transform', 'rotate(-90)')
            .attr('style', 'font-weight:700; font-size: 1.2rem;')
            .attr('x', 0)
            .attr('y', 5)
            .attr('dy', '.71em')
            .style('text-anchor', 'end')
            .text('PC2');

        // Circles
        var circles = svg.selectAll('circle')
            .data(data)
            .enter()
            .append('circle')
            .filter(function (d) {
                return config.selected_emotions.indexOf(d.emotion) >= 0;
            })
            .attr('cx', function (d) {
                return xScale(d[x_var])
            })
            .attr('cy', function (d) {
                return yScale(d[y_var])
            })

            .attr('ID', function (d) {
                return d['filename']
            })
            .attr('r', function (d) {
                return 5;
            })
            .attr('stroke-width', 2)
            .attr('fill', function (d) {
                return colors[d['emotion']]
            })
            .on('mouseover', function (p) {
                d3.select(this)
                    .transition()
                    .duration(100)
                    .attr('r', function (d) {
                        return 15;
                    });
                var ID = d3.select(this).attr("ID");
                var file_path = 'audio/' + config['corpus'] + '/' + ID.split('.')[0] + '.wav';
                play(file_path);

            })
            .on('mouseout', function () {
                d3.select(this)
                    .transition()
                    .duration(500)
                    .attr('r', function (d) {
                        return 5;
                    })
            })
            .append('title');
    });

}


window.onload = function () {
    get_GET_params = function (queryDict = {}) {
        location.search.substr(1).split("&").forEach(function (item) {
            queryDict[item.split("=")[0]] = item.split("=")[1]
        });
        return (queryDict)
    };

    set_GET_params = function (queryDict) {
        var new_url = location.pathname + "?";
        var params = [];
        var count = 0;
        for (const [key, value] of Object.entries(queryDict)) {
            params[count] = key + "=" + value
            count += 1
        }
        new_url += params.join('&')
        window.history.pushState('page2', 'Title', new_url);
    };


    var queryDict = {
        'corpus': document.querySelector('#corpus').value
    };
    queryDict = get_GET_params(queryDict);

    if (queryDict["model"] == null){
        window.model = "eGeMAPS";
    } else {
        window.model = queryDict["model"];
    }

    var options = document.getElementById('corpus').options;
    var index = null;
    for (var i = 0, len = options.length; i < len; i++) {
        if (options[i].value === queryDict.corpus) {
            document.querySelector('#corpus').selectedIndex = i;
            break;
        }
    }

    function collect_fields_and_refresh() {
        var d = {
            'corpus': document.querySelector('#corpus').value
        };

        var emotions = [];
        if (d['corpus'] === 'PELL') {
            emotions = ['NEU', 'FER', 'HAP', 'SAD', 'SUR', 'DIS', 'ANG'];
        } else if (d['corpus'] === 'RAVDESS') {
            emotions = ['NEU', 'FER', 'HAP', 'SAD', 'SUR', 'DIS', 'ANG', 'CAL'];
        } else {
            alert('Corpus not supported')
        }

        var selected_emotions = [];
        var count = 0;
        for (var i = 0, len = emotions.length; i < len; i++) {
            var emo = emotions[i];
            if (document.querySelector('#' + emo).checked) {
                selected_emotions[count] = emo;
                count += 1
            }
        }

        d['emotions'] = emotions;
        d['selected_emotions'] = selected_emotions;
        d['model'] = window.model;

        plot_overview(d);
        redraw(d);
    }

    collect_fields_and_refresh();


    // Reload the map if the corpus is changed
    // var inputs = document.querySelectorAll('select');
    // for (var i = 0, len = inputs.length; i < len; i++) {
    //     inputs[i].onchange = function () {
    //         collect_fields_and_refresh();
    //     };
    // }

    function refresh(selector) {

        var inputs = document.querySelectorAll(selector);

        for (var i = 0, len = inputs.length; i < len; i++) {
            inputs[i].onchange = function () {
                collect_fields_and_refresh();
            };
        }
    }

    var selectors = ['select', 'input'];
    selectors.forEach(refresh);

    // TODO filter emotions

    //
    //var selectors = ['select', 'input'];
    //selectors.forEach(warn_user);

    var radios = document.querySelectorAll(".uk-radio");
    for (var i = 0, len = radios.length; i < len; i++) {
        radios[i].onclick = function () {
            if (this.value === 'raw') {
                document.getElementById("filter_intention").classList.add('uk-hidden')
            } else {
                document.getElementById("filter_intention").classList.remove('uk-hidden')
            }
        }
    }
};
