# How to use

Viewing the map on your computer is easy.



First download the map

``` 
git clone https://gitlab.gwdg.de/pvanrij/model-performance
```



Then go to your repository

```
cd model-performance
```



If you have Python 3, run:

```
python3 -m http.server 9000
```



If you have Python 2, run:

```
python -m SimpleHTTPServer 9000
```



Now click on the link (for me it was `http://0.0.0.0:9000/`). Please use a compatible browser. The map has been tested for Firefox and Chrome.